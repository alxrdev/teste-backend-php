# TESTE PHP PHIDELIS

O objetivo desse teste é testar o candidato em algumas das stacks exigidas para a vaga de PHP.

## O que será analisado?

- Análise e compreensão do escopo
- Lógica e domínio sobre a liguagem PHP
- Lógica e domínio sobre o wordpress
- Organização do projeto e do código
- Performance e velocidade de entrega
- Qualidade da entrega

## Resumo do escopo
- Fazer um Fork do projeto base do teste (https://bitbucket.org/carlosarf/teste-backend-php/src)
- Corrigir um bug proposital existente no slider
- Criar um tema Wordpress
- Utilizar o plugin ACF para customização dos campos
- Tornar todo o conteúdo dinâmico (editáveis via painel)
- Criar pelo menos um post type
- Incluir o Banco de dados no repositório
- Abrir um PULL Request do projeto editado por você

## Entrega
- Teste todos os requisitos para uma entrega com qualidade. Este ponto será avaliado pelo nosso time;
- Realizar o Pull Request do seu projeto;
- Se desejar, descreva brevemente o critérios adotados por você no seu projeto;


## Como usar
- O tema wordpress está na pasta 'wp-theme'

- Optei por exportar os custom fields para um arquivo PHP e carregalos com um hook do próprio plugin ACF. Os campos estão no arquivo 'wp-theme/inc/acf-groups.php'. 

- É necessário ter o plugin **Advanced Custom Fields** instalado, criar uma página e definir como **front page** no menu 'Settings > Reading > Your homepage displays'. Após isso, os campos customisáveis apareceram na página para poder customizar.

- Também será necessário adicionar o menu de navegação primário em 'Appearance > Menus'.

- Também tem a opção de adicionar uma logo no painel de customização em 'Appearance > Customize > Site Identity'.

- Crie também uma pagina chamada 'Services' para utilizar a custom page que exibe todos os custom posts do tipo service.