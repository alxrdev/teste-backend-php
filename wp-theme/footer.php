<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Phidelis
 * @since 1.0.0
 */

?>

	<footer>
		<div class="container">
			<p>
				<?php ( get_field( 'footer_text' ) ) ? the_field( 'footer_text' ) : _e( '&copy; Your Website 2020. All Rights Reserved.', 'phidelis' ); ?>
			</p>
			<ul class="list-inline">
				<?php
					if ( have_rows( 'footer_items_list' ) ): 
						while ( have_rows( 'footer_items_list' ) ):
							phidelis_display_footer_item( the_row( true ) );
						endwhile;
					endif;
				?>
			</ul>
		</div>
	</footer>
	
	<?php wp_footer(); ?>
</body>
</html>