<?php
/**
 * Displays the page header
 *
 * @package Phidelis
 * @since 1.0.0
 */
?>

<div
    class="page-header <?php echo ( has_post_thumbnail() ) ? 'post-background' : ''; ?>"
    style="<?php echo ( has_post_thumbnail() ) ? 'background-image: url(' . phidelis_get_post_thumbnail( get_the_id() ) . ');' : ''; ?>"
>
    <div class="container">
        <h1>
            <?php wp_title( '' ); ?>
        </h1>
    </div>
</div>