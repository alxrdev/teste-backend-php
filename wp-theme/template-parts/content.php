<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Phidelis
 * @since 1.0.0
 */

?>

<a 
    href="<?php echo esc_url( get_permalink() ); ?>"
    class="post-item <?php echo ( has_post_thumbnail() ) ? 'has-thumbnail': ''; ?>"
>
    <?php if ( has_post_thumbnail() ): ?>
        <img src="<?php echo phidelis_get_post_thumbnail( get_the_id() ); ?>" alt="<?php echo get_the_title(); ?>">
    <?php endif; ?>

    <div class="content">
        <?php the_title( '<h3 class="title">', '</h3>' ); ?>
        <?php echo get_the_excerpt(); ?>
    </div>
</a>