<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Phidelis
 * @since 1.0.0
 */

?>

<section>
    <div class="container">
        <p>
            <?php _e( 'No content found.', 'phidelis' ); ?>
        </p>
    </div>
</section>