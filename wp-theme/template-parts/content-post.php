<?php
/**
 * Displays a single post
 *
 * @package Phidelis
 * @since 1.0.0
 */
?>

<section <?php post_class(); ?> id="post-<?php the_ID(); ?>">
    <?php get_template_part( 'template-parts/page-header' ); ?>
    
    <div class="container">
        <div class="page-content">
            <?php the_content(); ?>
        </div>
    </div>
</section>