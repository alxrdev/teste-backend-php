<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Phidelis
 * @since 1.0.0
 */

get_header();
?>

    <section class="404-page">
        <div class="container d-flex align-items-center" style="min-height:50vh;">
            <div>
                <h1 class="section-heading">
                    <?php _e( 'Nothing here', 'phidelis' ); ?>
                </h1>

                <p>
                    <?php esc_html_e( 'It looks like nothing was found at this location.', 'phidelis' ); ?>
                </p>
            </div>
        </div>
    </section>

<?php
get_footer();