<?php
/**
 * Custom template tags for this theme.
 *
 * @package Phidelis
 * @since 1.0.0
 */

/**
 * Displays the site logo or site title.
 *
 * @since  1.0.0
 * @return string $html Compiled HTML based on our arguments.
 */
function phidelis_logo() {
    $logo = get_custom_logo();
    $title = get_bloginfo( 'name' );

    if ( has_custom_logo() ) {
        echo $logo;
    } else {
        echo $title;
    }
}

/**
 * Displays the slides.
 *
 * @since  1.0.0
 * @param array $slides The slides array
 * @return string Compiled HTML based on our arguments.
 */
function phidelis_display_slider_items( array $slides ) {
    foreach ( $slides as $slide ) {
        ?>
        <div class="swiper-slide">
            <div class="row">
                <div class="col-lg-7 my-auto">
                    <div class="header-content mx-auto">
                        <h1 class="mb-5">
                            <?php echo $slide['slide_text']; ?>
                        </h1>
                        <?php if ( $slide['has_button'] ): ?>
                            <a href="<?php echo $slide['button_link']; ?>" class="btn btn-outline btn-xl js-scroll-trigger"><?php echo $slide['button_text']; ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-5 my-auto">
                    <?php if ( $slide['has_image'] ): ?>
                        <div class="device-container">
                            <div class="device-mockup <?php echo $slide['device_type']; ?> <?php echo $slide['device_orientation']; ?> <?php echo $slide['device_color']; ?>">
                                <div class="device">
                                    <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="<?php echo $slide['device_screen']; ?>" class="img-fluid" alt="">
                                    </div>
                                    <div class="button">
                                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
    }
}

/**
 * Displays a features row.
 *
 * @since  1.0.0
 * @param array $features A features array
 * @return string Compiled HTML based on our arguments.
 */
function phidelis_display_feature_row( array $features ) {
    foreach ( $features as $feature ) {
        ?>
        <div class="col-lg-6">
            <div class="feature-item">
                <i class="<?php echo $feature['icon']; ?> text-primary"></i>

                <h3><?php echo $feature['title']; ?></h3>

                <p class="text-muted"><?php echo $feature['subtitle']; ?></p>
            </div>
        </div>
        <?php
    }
}

/**
 * Displays a social media item.
 *
 * @since  1.0.0
 * @param array $social_medias The social media items to display
 * @return string Compiled HTML based on our arguments.
 */
function phidelis_display_social_media_item( array $social_medias ) {
    $social_media_icons = array(
        'twitter' => 'fa-twitter',
        'facebook' => 'fa-facebook-f',
        'google-plus' => 'fa-google-plus-g'
    );

    foreach ( $social_medias as $social_media ) {
        ?>
            <li class="list-inline-item social-<?php echo $social_media['social_media_name']; ?>">
                <a href="<?php echo $social_media['link']; ?>" target="_blank">
                    <i class="fab <?php echo $social_media_icons[$social_media['social_media_name']]; ?>"></i>
                </a>
            </li>
        <?php
    }
}

/**
 * Displays a footer item.
 *
 * @since  1.0.0
 * @param array $footer_items The footer items to display
 * @return string Compiled HTML based on our arguments.
 */
function phidelis_display_footer_item( array $footer_items ) {
    foreach ( $footer_items as $footer_item ) {
        ?>
            <li class="list-inline-item">
                <a href="<?php echo $footer_item['link']; ?>">
                    <?php echo $footer_item['text']; ?>
                </a>
            </li>
        <?php
    }
}

/**
 * Returns the post thumbnail.
 *
 * @since  1.0.0
 * @param array $post_id The post id
 * @return string Thumbnail URL
 */
function phidelis_get_post_thumbnail( $post_id ) {
    return wp_get_attachment_url( get_post_thumbnail_id( $post_id ) );
}