<?php
/**
 * The front page template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Phidelis
 * @since 1.0.0
 */

get_header();
?>

	<!-- Download section -->
	<section class="download text-center <?php echo ( get_field( 'download_section_bg' ) ) ? get_field( 'download_section_bg' ) : '.bg-primary'; ?>" id="download">
		<div class="container">
			<div class="row">
				<div class="col-md-8 mx-auto">
					<h2 class="section-heading">
						<?php ( get_field( 'download_section_title' ) ) ? the_field( 'download_section_title' ) : _e( 'Discover what all the buzz is about!', 'phidelis' ); ?>
					</h2>
					
					<p>
						<?php ( get_field( 'download_section_subtitle' ) ) ? the_field( 'download_section_subtitle' ) : _e( 'Our app is available on any mobile device! Download now to get started!', 'phidelis' ); ?>
					</p>
					
					<div class="badges">
						<?php if ( get_field( 'has_google_play_link' ) ) : ?>
							<a class="badge-link" href="<?php the_field( 'google_play_link' ); ?>" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/google-play-badge.svg" alt="Google Play">
							</a>
						<?php endif; ?>

						<?php if ( get_field( 'has_apple_store_link' ) ) : ?>
							<a class="badge-link" href="<?php the_field( 'apple_store_link' ); ?>" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/app-store-badge.svg" alt="Apple Play">
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Features section -->
	<section class="features <?php ( get_field( 'features_section_bg' ) ) ? the_field( 'features_section_bg' ) : ''; ?>" id="features">
		<div class="container">
			<div class="section-heading text-center">
				<h2>
					<?php ( get_field( 'features_section_title' ) ) ? the_field( 'features_section_title' ) : 'Unlimited Features, Unlimited Fun'; ?>
				</h2>
				
				<p class="text-muted">
					<?php ( get_field( 'features_section_subtitle' ) ) ? the_field( 'features_section_subtitle' ) : 'Check out what you can do with this app theme!'; ?>
				</p>
				<hr>
			</div>

			<div class="row">
				<div class="col-lg-4 my-auto">
					<div class="device-container">
						<?php 
							if ( have_rows( 'features_section_device' ) ) :
								while ( have_rows( 'features_section_device' ) ) : the_row(); ?>
									<div class="device-mockup <?php the_sub_field( 'device_type' ); ?> <?php the_sub_field( 'device_orientation' ); ?> <?php the_sub_field( 'device_color' ); ?>">
										<div class="device">
											<div class="screen">
												<!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
												<img src="<?php the_sub_field( 'device_screen' ); ?>" class="img-fluid" alt="">
											</div>
											<div class="button">
												<!-- You can hook the "home button" to some JavaScript events or just remove it -->
											</div>
										</div>
									</div>
								<?php endwhile;
							endif;
						?>
					</div>
				</div>

				<div class="col-lg-8 my-auto">
					<div class="container-fluid">
						<div class="row">
							<?php
								if ( have_rows( 'features_section_first_row' ) ) :
									while ( have_rows( 'features_section_first_row' ) ) :
										phidelis_display_feature_row( the_row( true ) );
									endwhile;
								endif;
							?>
						</div>

						<div class="row">
							<?php
							 if ( have_rows( 'features_section_second_row' ) ) :
								while ( have_rows( 'features_section_second_row' ) ) :
									phidelis_display_feature_row( the_row( true ) );
								endwhile;
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Call to action section -->
	<section
		class="cta"
		style="<?php echo ( get_field( 'cta_section_bg' ) ) ? 'background-image: url(' . get_field( "cta_section_bg" ) . ')' : ''; ?>"
	>
		<div class="cta-content">
			<div class="container">
				<h2>
					<?php ( get_field( 'cta_section_title' ) ) ? the_field( 'cta_section_title' ) : _e( 'Stop waiting.<br>Start building.', 'phidelis' ); ?>
				</h2>

				<?php
					if ( get_field( 'cta_section_has_action_button' ) ): ?>
						<a href="<?php echo esc_url( get_field( 'cta_section_action_button_link' ) ); ?> " class="btn btn-outline btn-xl js-scroll-trigger">
							<?php the_field( 'cta_section_action_button_text' ); ?>	
						</a>
					<?php endif;
				?>
			</div>
		</div>
		<div class="overlay"></div>
	</section>

	<!-- Contact section -->
	<section class="contact <?php echo ( get_field( 'contact_section_bg' ) ) ? get_field( 'contact_section_bg' ) : 'bg-primary' ?>" id="contact">
		<div class="container">
			<h2>
				<?php ( get_field( 'contact_section_title' ) ) ? the_field( 'contact_section_title' ) : _e( 'We <i class="fas fa-heart"></i> new friends!', 'phidelis' ) ; ?>
			</h2>
			
			<ul class="list-inline list-social">
				<?php
					if ( have_rows( 'contact_section_social_medias' ) ):
						while ( have_rows( 'contact_section_social_medias' ) ):
							phidelis_display_social_media_item( the_row(true) );
						endwhile;
					endif;
				?>
			</ul>
		</div>
	</section>

<?php
get_footer();