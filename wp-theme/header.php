<?php
/**
 * Header file.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Phidelis
 * @since 1.0.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <?php wp_head(); ?>

</head>

<body id="page-top" <?php body_class(); ?>>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top <?php echo ( !is_front_page() ) ? 'navbar-active' : '' ?>" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="<?php echo esc_url( home_url( '/' ) ); ?>#page-top">
        <?php phidelis_logo(); ?>
      </a>

      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <?php _e( 'Menu', 'phidelis' ); ?>
        <i class="fas fa-bars"></i>
      </button>

      <div class="collapse navbar-collapse" id="navbarResponsive">
        <?php
          if ( has_nav_menu ( 'primary' ) ):
            wp_nav_menu(
              array(
                'theme_location' => 'primary',
                'container' => false,
                'menu_class' => 'navbar-nav ml-auto',
                'menu_id' => '',
              )
            );
          else:
            ?>
            <div class="navbar-nav ml-auto">
              <span>Add a main menu here</span>
            </div>
            <?php
          endif;
        ?>
      </div>
    </div>
  </nav>

  <?php
    if ( is_front_page() ):
      ?>
      <header class="masthead">
        <div class="container h-100">
          <div class="row h-100">
            <div class="col-lg-12 my-auto">
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <?php
                    if ( have_rows( 'slider_list' ) ): 
                      while ( have_rows( 'slider_list' ) ):
                        phidelis_display_slider_items( the_row( true ) );
                      endwhile;
                    endif;
                  ?>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
              
            </div>
          </div>
        </div>
      </header>
      <?php
    endif;
  ?>