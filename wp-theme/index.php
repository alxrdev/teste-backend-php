<?php
/**
 * The main template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Phidelis
 * @since 1.0.0
 */

get_header();
?>

<section class="posts-page">
	<?php get_template_part( 'template-parts/page-header' ); ?>
	
	<div class="container">
		<div class="content">
			<?php 
				if ( have_posts() ) {

					while ( have_posts() ) {
						the_post();
						get_template_part( 'template-parts/content' );
					}

				} else {
				
					get_template_part( 'template-parts/content', 'none' );
				
				}
			?>
		</div>
	</div>
</section>

<?php
get_footer();