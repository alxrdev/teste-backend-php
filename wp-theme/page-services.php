<?php
/**
 * Template Name: Services post template
 * Template Post Type: post
 *
 * @package Phidelis
 * @since 1.0
 */

get_header();
?>

<section class="posts-page">
	<?php get_template_part( 'template-parts/page-header' ); ?>
	
	<div class="container">
		<div class="content">
			<?php 
				$loop = new WP_Query(
					array(
						'post_type'      => 'service',
						'posts_per_page' => 10,
					)
				);

				if ( $loop->have_posts() ) {

					while ( $loop->have_posts() ) {
						$loop->the_post();
						get_template_part( 'template-parts/content' );
					}

				} else {
				
					get_template_part( 'template-parts/content', 'none' );
				
				}

				wp_reset_postdata();
			?>
		</div>
	</div>
</section>

<?php
get_footer();