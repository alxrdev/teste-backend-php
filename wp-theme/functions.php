<?php
/**
 * Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Phidelis
 * @since 1.0.0
 */

 /**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function phidelis_theme_support() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1568, 9999 );

	// Add custom logo support
	add_theme_support(
		'custom-logo',
		array(
			'width'       => 120,
			'height'      => 90,
			'flex-height' => true,
			'flex-width'  => true,
			'unlink-homepage-logo' => true
		)
	);

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
	);

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 */
	load_theme_textdomain( 'phidelis' );

}

add_action( 'after_setup_theme', 'phidelis_theme_support' );

/**
 * Register and Enqueue Styles.
 */
function phidelis_register_styles() {

	$theme_version = wp_get_theme()->get( 'Version' );

    // Bootstrap core CSS
    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css', array(), $theme_version );
    
    // Custom fonts for this template
	wp_enqueue_style( 'fontawesome-style', get_template_directory_uri() . '/assets/vendor/fontawesome-free/css/all.min.css', array(), $theme_version );
	wp_enqueue_style( 'swiper-style', get_template_directory_uri() . '/assets/vendor/swiper/swiper-bundle.min.css', array(), $theme_version );
	wp_enqueue_style( 'simple-style', get_template_directory_uri() . '/assets/vendor/simple-line-icons/css/simple-line-icons.css', array(), $theme_version );
	wp_enqueue_style( 'google-lato-font', 'https://fonts.googleapis.com/css?family=Lato', array(), $theme_version );
	wp_enqueue_style( 'google-catamaran-style', 'https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900', array(), $theme_version );
	wp_enqueue_style( 'google-muli-font', 'https://fonts.googleapis.com/css?family=Muli', array(), $theme_version );
    
    // Plugin CSS
    wp_enqueue_style( 'device-mockups-style', get_template_directory_uri() . '/assets/device-mockups/device-mockups.min.css', array(), $theme_version );
    
    // Custom styles for this template
    wp_enqueue_style( 'phidelis-style', get_template_directory_uri() . '/assets/css/new-age.min.css', array(), $theme_version );

}

add_action( 'wp_enqueue_scripts', 'phidelis_register_styles' );

/**
 * Register and Enqueue Scripts.
 */
function phidelis_register_scripts() {

	$theme_version = wp_get_theme()->get( 'Version' );

    // Bootstrap core JavaScript
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/vendor/jquery/jquery.min.js', array(), $theme_version, true );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.bundle.min.js', array( 'jquery' ), $theme_version, true );
    
    // Plugin JavaScript
    wp_enqueue_script( 'jquery-easing-js', get_template_directory_uri() . '/assets/vendor/jquery-easing/jquery.easing.min.js', array( 'jquery' ), $theme_version, true );
    
    wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/assets/vendor/swiper/swiper-bundle.min.js', array(), $theme_version, true );
    
    // Custom scripts for this template
    wp_enqueue_script( 'phidelis-js', get_template_directory_uri() . '/assets/js/new-age.min.js', array( 'jquery', 'swiper-js' ), $theme_version, true );

}

add_action( 'wp_enqueue_scripts', 'phidelis_register_scripts' );

/**
 * Register a services post type 
 */
function phidelis_register_services_post_type() {
	$args = array(
		'label' 			 => __( 'Services', 'phidelis' ),
		'labels' 			 => array(
			'name'				=> __( 'Services', 'phidelis' ),
			'singular_name' 	=> __( 'Service', 'phidelis' ),
			'menu_name'             => __( 'Services', 'phidelis' ),
			'name_admin_bar'        => __( 'Services', 'phidelis' ),
			'add_new'               => __( 'Add New', 'phidelis' ),
			'add_new_item'          => __( 'Add New service', 'phidelis' ),
			'new_item'              => __( 'New service', 'phidelis' ),
			'edit_item'             => __( 'Edit service', 'phidelis' ),
			'view_item'             => __( 'View service', 'phidelis' ),
			'all_items'             => __( 'All services', 'phidelis' ),
			'search_items'          => __( 'Search services', 'phidelis' ),
			'parent_item_colon'     => __( 'Parent services:', 'phidelis' ),
			'not_found'             => __( 'No services found.', 'phidelis' ),
			'not_found_in_trash'    => __( 'No services found in Trash.', 'phidelis' ),
			'featured_image'        => __( 'Service Cover Image', 'phidelis' ),
			'set_featured_image'    => __( 'Set cover image', 'phidelis' ),
			'remove_featured_image' => __( 'Remove cover image', 'phidelis' ),
			'use_featured_image'    => __( 'Use as cover image', 'phidelis' ),
			'insert_into_item'      => __( 'Insert into service', 'phidelis' ),
			'uploaded_to_this_item' => __( 'Uploaded to this service', 'phidelis' ),
			'filter_items_list'     => __( 'Filter services list', 'phidelis' ),
			'items_list_navigation' => __( 'Services list navigation', 'phidelis' ),
			'items_list'            => __( 'Services list', 'phidelis' ),
		),
		'description' 		 => __( 'A services post type', 'phidelis' ),
		'public' 			 => true,
		'show_ui'            => true,
		'show_in_menu' 		 => true,
		'show_in_nav_menus'	 => true,
		'show_in_admin_bars' => true,
		'show_in_rest' 		 => true,
		'menu_position' 	 => 5,
		'supports' 				 => array( 'title', 'editor', 'author', 'thumbnail' ),
		'taxonomies'			 => array( 'category', 'post_tag' ),
		'has_archive' 			 => false,
		'rewrite' 				 => array('slug' => 'services'),
	);

	register_post_type( 'service', $args );
}

add_action( 'init', 'phidelis_register_services_post_type' );

/**
 * Register navigation menus.
 */
function phidelis_menus() {

	$locations = array(
		'primary'  => __( 'Main Menu', 'phidelis' )
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'phidelis_menus' );

/**
 * Add custom class on li nav menu.
 */
function phidelis_add_class_on_li_nav_menu( $classes, $item, $args ) {
    $classes = array( 'nav-item' );
    return $classes;
}

add_filter( 'nav_menu_css_class', 'phidelis_add_class_on_li_nav_menu', 1, 3 );

/**
 * Add custom class on a link on nav menu.
 */
function phidelis_add_class_on_nav_menu_link( $ul_class ) {
	return preg_replace('/<a /', '<a class="nav-link js-scroll-trigger"', $ul_class);
}

add_filter( 'wp_nav_menu', 'phidelis_add_class_on_nav_menu_link' );

/**
 * Action to initialize scripts only in the front page
 */
function phidelis_init_scripts_on_front_page() {
	if ( is_front_page() ):
		?>
		<!-- Initialize Swiper -->
		<script>
			var swiper = new Swiper('.swiper-container', {
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
		</script>
		<?php
	endif;
}

add_action( 'wp_footer', 'phidelis_init_scripts_on_front_page', 100 );

/**
 * Show a notice to require all dependencies
 */
function phidelis_plugin_dependencies() {
	if( ! function_exists('have_rows') ) {
		printf( '<div class="error"><p> %s </p></div>', __( 'Warning: This theme needs The <a href="' . esc_url( 'https://wordpress.org/plugins/advanced-custom-fields/' ) . '" target="_blank">Advanced Custom Fields</a>', 'phidelis' ) );
  	}
}

add_action( 'admin_notices', 'phidelis_plugin_dependencies' );

// Required files
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/acf-groups.php';